### Deploy autoscaling component

    kubectl apply -f https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml

### Check autoscaling component
    kubectl get deployment -n kube-system cluster-autoscaler

### Check autoscaling component
    kubectl edit deployment -n kube-system cluster-autoscaler

### Add components below to the autoscaling file
    deployment.kubernetes.io/revision: "1"
    **cluster-autoscaler.kubernetes.io/safe-to-evict: "false"**

    - --node-group-auto-discovery=asg:tag=k8s.io/cluster-autoscaler/enabled,k8s.io/cluster-autoscaler/**eks-cluster-test**
    **- --balance-similar-node-groups
    - --skip-nodes-with-system-pods=false**

    image: registry.k8s.io/autoscaling/cluster-autoscaler:v**1.29.3**

### Verify the kubernetes version in AWS and with the link below
    https://github.com/kubernetes/autoscaler/tags

### Deploy nginx pods with service

    kubectl apply -f nginx.yaml
